Bài Capstone Bootstrap của nhóm 5

Thành viên:
- Lê Danh Phương (Leader)
- Lê Danh
- Nguyễn Minh Trí

Link Đề bài: 
https://demo.w3layouts.com/demos_new/template_demo/03-03-2021/masterwork-liberty-demo_Free/423112365/web/index.html?fbclid=IwAR1hMJo4U-vB-6itXuBLAqXs8mbuFqkWu5VUiDIAmgCYbUZ6BgakrWx-uWo

Link Deploy Website:
https://bc05-nhom5-capstone-bootstrap.netlify.app/

Link Video:
https://www.youtube.com/watch?v=D4cwCv22M84

Danh sách chia công việc:
https://docs.google.com/spreadsheets/d/1jS_WZadgD3k6nTwuoDIHuBTlh9AbIUzUz-OOV5uvSNg/edit?usp=sharing
